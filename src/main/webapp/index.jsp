<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body style="width: 70%; margin: auto">
<h2>Hello World !</h2>

<form action="index.jsp" method="post">
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"
               placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1" name="checkbox">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <br>
    <div class="form-group">
        <label for="colors">Check color:</label>
        <select name="colors" class="form-control" id="colors">
            <option value="red">Czerwony</option>
            <option value="green">Zielony</option>
            <option value="blue">Niebieski</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

Hello! The time is now <%= new java.util.Date() %>
<br>
Z uzyciem expression:
<br>
Obecna data: <%=  LocalDate.now() %>

<br>
Z uzyciem skryptletu:
<br>
Skryptlet: Dzisiaj jest: <% out.println(java.time.LocalDate.now().toString()); %>

<br>
Z użyciem zmiennej:
<br>
<%
    System.out.println("Evaluating date now");
    java.util.Date date = new java.util.Date();
%>
Hello! The time is now <%= date %>

<br>
<br>
Dynamiczne rysowanie tabelki:
<br>
<table border="2" class="table">
    <% for (int i = 0; i < 10; i++) {%>
    <tr>
        <td>Number</td>
        <td><%= i + 1 %>
        </td>
    </tr>
    <% } %>
</table>
<br>

<br>
Działanie IF:
<br>
<% boolean hello = false; %>
<% if (hello) {%>
<p>Hello World! TRUE</p>
<% } else { %>
<p>Goodbye, world! FALSE</p>
<% }%>

<br>
Dyrektywy (import):
<br>
<% Date dateImport = new Date(); %>
Hello! The time is now: <%= dateImport %>
<br>
<br>
Import pliku hello.jsp
<br>
<%@ include file="hello.jsp" %>
<br>
<br>
Deklaracja zmiennej visitCount
<br>
<%! private long visitCount = 0; %>
<h2>Ilość odwiedzin strony: <%= ++visitCount %>
</h2>
<br>
<br>
Metoda:
<br>
<%! Date theDate = new Date();

    Date getDate() {
        System.out.println("In getDate() method");
        return theDate;
    } %>
Hello! Data za pomocą metody getDate(): <%= getDate()  %>
<br>
<%--
Komentarz JSP wielolinijkowy
--%>
<!--
Komentarz HTML
-->
<br>
Znacznik akcji INCLUDE:
<br>
<jsp:include page="hello.jsp"/>
<br>
Znacznik akcji FORWARD:
<br>
(zakomentowane aby nie przekierowało)
<br>
<%--<jsp:forward page="hello.jsp"/>--%>
<br>
Dyrektywa taglib (biblioteki JSTL, core):
<br>
<c:set var="salary" scope="session" value="${2000*2}"/>
<p>My salary is:  <c:out value="${salary}"/><p>
    <c:if test="${salary < 5000}">
<h3>Wyświetlam bo wartość TRUE!</h3>
</c:if>
<br>
Znaczniki c:choose, c:when
<br>
<c:choose>
    <c:when test="${salary > 2000}">
        ${salary} większe od 2000
    </c:when>
    <c:when test="${salary < 7000}">
        ${salary} mniejsze od 7000
    </c:when>
</c:choose>
<br>
<br>
Znacznik c:forEach
<c:set var="alphabet"
       value="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']}"
       scope="application"/>
<br>
<c:forEach var="varName" begin="0" end="10">
    ${varName}
</c:forEach>
<br>
<c:forEach var="varName" begin="0" end="10">
    ${varName}=${alphabet[varName]}
</c:forEach>
<br>
<c:forEach items="${alphabet}" var="varName" begin="0" end="10">
    ${varName}
</c:forEach>
<br>
<c:forEach items="${alphabet}" var="varName">
    ${varName}
</c:forEach>
<br>
<c:forEach items="${alphabet}" varStatus="obiekt">
    ${obiekt.current}
</c:forEach>
<br>
<br>
Funkcje prefix fn:
<br>
size of alphabet = ${fn:length(alphabet)}
<br>
indeks litery "t" w słowie alphabet: ${fn:indexOf("alphabet","t")}
<br>
<br>
Formatowanie prefix fmt:
<br>
<fmt:formatNumber value="${salary}" type="currency"/>
<br>
<h3>Number Format:</h3>
<c:set var="balance" value="120000.2309"/>
<p>Liczba: ${balance}</p>
<p>Formatted Number (1) type = "currency":
    <fmt:formatNumber value="${balance}" type="currency"/></p>

<p>Formatted Number (2) type = "number" maxIntegerDigits = "3":
    <fmt:formatNumber type="number" maxIntegerDigits="3" value="${balance}"/></p>

<p>Formatted Number (3) type = "number" maxFractionDigits = "3":
    <fmt:formatNumber type="number" maxFractionDigits="3" value="${balance}"/></p>

<p>Formatted Number (4) type = "number" groupingUsed = "false":
    <fmt:formatNumber type="number" groupingUsed="false" value="${balance}"/></p>

<p>Formatted Number (5) type = "percent" maxIntegerDigits="3":
    <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${balance}"/></p>

<p>Formatted Number (6) type = "percent" minFractionDigits = "10":
    <fmt:formatNumber type="percent" minFractionDigits="10" value="${balance}"/></p>

<p>Formatted Number (7) type = "percent" maxIntegerDigits = "3":
    <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${balance}"/></p>

<p>Formatted Number (8) type = "number" pattern = "###.###E0":
    <fmt:formatNumber type="number" pattern="###.###E0" value="${balance}"/></p>
<p>Waluta w PL :
    <fmt:setLocale value="pl_PL"/>
    <fmt:formatNumber value="${balance}" type="currency"/>
</p>
<p>Currency in USA :
    <fmt:setLocale value="en_US"/>
    <fmt:formatNumber value="${balance}" type="currency"/>
</p>
<br>
<h3>Number Parsing:</h3>
<c:set var="balance" value="1250003.350"/>
<p>Liczba: ${balance}</p>
<fmt:parseNumber var="i" type="number" value="${balance}"/>
<p>Parsed Number (1) type = "number": <c:out value="${i}"/></p>

<fmt:parseNumber var="i" integerOnly="true" type="number" value="${balance}"/>
<p>Parsed Number (2) integerOnly = "true" type = "number": <c:out value="${i}"/></p>
<br>
<br>
<h3>Request:</h3>
<h5>Request/GET (?query=jsp&page=3&sort=desc) java:</h5>
query: <%= request.getParameter("query") %>
<br>
page: <%= request.getParameter("page") %>
<br>
sort: <%= request.getParameter("sort") %>
<br>
<h5>Request/GET (?query=jsp&page=3&sort=desc) wyrażenia:</h5>
query: <c:out value="${param.get('query')}"/>
<br>
page: <c:out value="${param.get('page')}"/>
<br>
sort: <c:out value="${param.get('sort')}"/>
<br>
<h5>Request/POST z formularza na górze strony java:</h5>
email: <%= request.getParameter("email") %>
<br>
password: <%= request.getParameter("password") %>
<br>
chceckbox: <%= request.getParameter("checkbox") %>
<br>
color: <%= request.getParameter("colors") %>
<br>
<br>
<h3>ForEach na parametrach POST (wyrażenia):</h3>
<c:set var="parametry" value="${param.entrySet()}"/>
<c:forEach items="${parametry}" varStatus="obiekt">
    ${obiekt.current}<br>
</c:forEach>
<br>
<h3>Ciasteczka:</h3>
<%
    Cookie cookie = new Cookie("searchId", String.valueOf(2138902773));
    cookie.setMaxAge(60 * 60 * 24);
    response.addCookie(cookie);
%>
Ciasteczko name: <%= cookie.getName() %><br>
Ciasteczko value: <%= cookie.getValue() %><br>
Ciasteczko domain: <%= cookie.getDomain() %><br>
<br>
<br>
<h3>Licznik na ciasteczku:</h3>
<% Cookie odwiedziny = null;
    odwiedziny = new Cookie("odwiedziny", String.valueOf(1));
    int odwiedzinyLiczba = 1;

    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookieLicznik : cookies) {
            if ("odwiedziny".equals(cookieLicznik.getName())) {
                odwiedzinyLiczba = Integer.parseInt(cookieLicznik.getValue()) + 1;
                odwiedziny = new Cookie("odwiedziny", String.valueOf(odwiedzinyLiczba));
            }
        }
    }
    response.addCookie(odwiedziny);

%>
Odwiedziny nr: <%= odwiedzinyLiczba %>
<br>
<br>
<h3>Licznik przy użyciu sesji:</h3>
<%
    Integer tmpLicznik = 1;
    if (!session.isNew()) {
        tmpLicznik = (Integer) session.getAttribute("licznikNaSesji");
        tmpLicznik = tmpLicznik + 1;
    }
    session.setAttribute("licznikNaSesji", tmpLicznik);
%>
odwiedziłeś nas po raz: <%=tmpLicznik %>
<%
    out.println("<br>getId: " + session.getId());
    out.println("<br>getCreationTime: " + session.getCreationTime());
    out.println("<br>getCreationTime: " + new Date(session.getCreationTime()));
    out.println("<br>getLastAccessedTime: " + session.getLastAccessedTime());
    out.println("<br>getLastAccessedTime: " + new Date(session.getLastAccessedTime()));
    out.println("<br>getMaxInactiveInterval: " + session.getMaxInactiveInterval());
    out.println("<br>getServletContext: " + session.getServletContext());
%>
<form action="wyloguj.jsp" method="post">
    <input type="hidden" value="zerujSesje">
    <button class="btn btn-primary" name="zerujSesje">Zeruj sesję</button>
</form>

<br>
<form action="<%= application.getContextPath() %>/licznik" method="post">
    <h2>Parametry do servletu app/licznik przekazane po POST:</h2>
    <div class="form-group">
        <label for="param1">Parametr 1:</label>
        <input type="text" class="form-control" name="param1" id="param1">
    </div>
    <div class="form-group">
        <label for="param2">Parametr 2:</label>
        <input type="text" class="form-control" name="param2" id="param2">
    </div>
    <button type="submit" class="btn btn-primary">Wejdź na servlet licznik i przekaż parametry</button>
</form>
<br>
<form action="<%= application.getContextPath() %>/Zadanie2" method="post">
    <h2>Parametry do servletu app/Zadanie2 przekazane po POST:</h2>
    <div class="form-group">
        <label for="param1">Parametr 1:</label>
        <input type="text" class="form-control" name="param1" id="param1">
    </div>
    <div class="form-group">
        <label for="param2">Parametr 2:</label>
        <input type="text" class="form-control" name="param2" id="param2">
    </div>
    <div class="form-group">
        <label for="param3">Parametr 3:</label>
        <input type="text" class="form-control" name="param3" id="param1">
    </div>
    <div class="form-group">
        <label for="param4">Parametr 4:</label>
        <input type="text" class="form-control" name="param4" id="param2">
    </div>
    <button type="submit" class="btn btn-primary">Wejdź na servlet Zadanie2 i przekaż parametry</button>
</form>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body style="width: 70%; margin: auto">

<form action="<%= application.getContextPath() %>/licznik" method="post">
    <h2>Parametry do servletu app/licznik przekazane po POST:</h2>
    <div class="form-group">
        <label for="param1">Parametr 1:</label>
        <input type="text" class="form-control" name="param1" id="param1">
    </div>
    <div class="form-group">
        <label for="param2">Parametr 2:</label>
        <input type="text" class="form-control" name="param2" id="param2">
    </div>
    <button type="submit" class="btn btn-primary">Wejdź na servlet licznik i przekaż parametry</button>
</form>
</body>
</html>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body style="width: 70%; margin: auto">
<h2>Logowanie !</h2>

<form action="<%= application.getContextPath() %>/register" method="post">
    <div class="form-group">
        <label for="text">Username:</label>
        <input type="text" class="form-control" id="text" name="user" placeholder="Enter username">
    </div>
    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="pass" placeholder="Enter password">
    </div>
    <div class="form-group">
        <label for="email">E-mail:</label>
        <input type="text" class="form-control" id="email" name="email" placeholder="Enter e-mail">
    </div>
    <div class="form-group">
        <label for="telephone">Telephone number:</label>
        <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Enter telephone">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<c:if test="${not empty blad}">
    <div class="alert alert-danger">
        Błędy w formularzu (JSP) <!-- JSP -->
    </div>
    <div class="alert alert-danger">
            ${blad} <!-- SERVLET -->
    </div>
</c:if>

</body>
</html>

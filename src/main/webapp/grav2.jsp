<%@ page import="java.util.Random" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body style="width: 70%; margin: auto">
<h2>Zgaduj liczbę z zakresu 0-99! ver.2</h2>
<%! Random rnd = new Random();%>
<%! private int losowa; %>
<%--<%=request.getMethod() %> DIAGNOSTYKA--%>
<%
    if(("GET").equals(request.getMethod())) {
        losowa = rnd.nextInt(100);
    }
%>
<c:set var="losowaLiczba" value="<%=losowa %>"/>

<%--init=${losowaLiczba}<br> DIAGNOSTYKA--%>

<form action="grav2.jsp" method="post">
    <div class="form-group">
        <label for="liczba">Wpisz liczbę:</label>
        <input type="text" class="form-control" id="liczba" name="liczba" placeholder="Wpisz liczbę">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
Twoja liczba: <%= request.getParameter("liczba") %>
<br>
<c:choose>
    <c:when test="${param.get('liczba') < losowaLiczba}">
        jest <b>MNIEJSZA</b> od wylosowanej
    </c:when>
    <c:when test="${param.get('liczba') > losowaLiczba}">
        jest <b>WIĘKSZA</b> od wylosowanej
    </c:when>
    <c:when test="${param.get('liczba') == losowaLiczba}">
        <b>Zgadłeś! Wylosowaną liczbą była liczba: ${losowaLiczba}</b>
    </c:when>
</c:choose>
<br>
<a href="grav2.jsp" class="link">Zagraj od nowa</a>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body style="width: 70%; margin: auto">
<h2>Wylogowano!</h2>
<%
    if (request.getParameter("zerujSesje") != null) {
        session.invalidate();
    }
%>
<a href="index.jsp" class="" link>Wróć do INDEX.JSP</a>

</body>
</html>

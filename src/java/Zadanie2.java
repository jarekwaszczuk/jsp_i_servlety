import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

@WebServlet(name = "Zadanie2", value = "/Zadanie2")
public class Zadanie2 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String naglowek = "<html>\n" +
                "<head>\n" +
                "    <link href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css\" rel=\"stylesheet\"/>\n" +
                "    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.1.1/css/all.css\"\n" +
                "          integrity=\"sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ\" crossorigin=\"anonymous\">\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "</head>\n" +
                "<body style=\"width: 70%; margin: auto\">";

        String stopka = "</body></html>";

        String formularz = "<br>" +
                "<form action=\"" + request.getContextPath() + "/Zadanie2\" method=\"post\">" +
                " <h2> Parametry do servletu app/Zadanie2 przekazane po POST:</h2>" +
                " <div class=\"form-group\">" +
                "    <label for=\"param1\" > Parametr 1:</label>" +
                "    <input type = \"text\" class=\"form-control\" name = \"param1\" id = \"param1\">" +
                " </div>" +
                " <div class=\"form-group\">" +
                "   <label for=\"param2\" > Parametr 2:</label>" +
                "    <input type =\"text\" class=\"form-control\" name =\"param2\" id =\"param2\">" +
                " </div>" +
                " <div class=\"form-group\">" +
                "     <label for=\"param3\" > Parametr 3:</label>" +
                "     <input type =\"text\" class=\"form-control\" name = \"param3\" id = \"param1\">" +
                "  </div>" +
                "  <div class=\"form-group\">" +
                "       <label for=\"param4\" > Parametr 4:</label>" +
                "       <input type =\"text\" class=\"form-control\" name =\"param4\" id =\"param2\">" +
                "  </div>" +
                "  <button type =\"submit\" class=\"btn btn-primary\" > Wejdź na servlet Zadanie2 i przekaż parametry</button>" +
                "</form>" +
                "<br>";

        response.getWriter().print(naglowek);
        response.getWriter().print(formularz);
        response.getWriter().print(stopka);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Map<String, String[]> paramMap = request.getParameterMap();

//        //foreach zwykły
//        for (Map.Entry<String, String[]> parameter : paramMap.entrySet()) {
//            response.getWriter().print("Parametr: " + parameter.getKey() + " ");
//            response.getWriter().print("Wartość: " + Arrays.toString(parameter.getValue()) + "<br>");
//        }

        //lambda i foreach
        paramMap.forEach((k, v) -> {
            try {
                response.getWriter().print("Parametr: " + k + " ");
                response.getWriter().print("Wartość: " + request.getParameter(k) + " ");
                response.getWriter().print("<br>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

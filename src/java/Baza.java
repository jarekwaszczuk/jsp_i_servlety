import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "baza", value = "/baza")
public class Baza extends HttpServlet {

//    public static void testBazy(HttpServletResponse response) throws ServletException, IOException {
//
//        try {
//            Class.forName("org.sqlite.JDBC");
//            response.getWriter().print("testBazy()");
//            String dbURL = "jdbc:sqlite:C:\\Users\\Jarek\\IdeaProjects\\jsp2\\resources\\database.db";
//            Connection conn = DriverManager.getConnection(dbURL);
//            if (conn != null) {
//                response.getWriter().print("Connected to the database");
//                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
//                response.getWriter().print("<br>Driver name: " + dm.getDriverName());
//                response.getWriter().print("<br>Driver version: " + dm.getDriverVersion());
//                response.getWriter().print("<br>Product name: " + dm.getDatabaseProductName());
//                response.getWriter().print("<br>Product version: " + dm.getDatabaseProductVersion());
//                conn.close();
//            }
//        } catch (ClassNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        }
//    }


//    private Connection con = null;
//
//    public static Connection ConnecrDb() throws IOException {
//
//        try {
//            //String dir = System.getProperty("user.dir");
//            Class.forName("org.sqlite.JDBC");
//            Connection con = DriverManager.getConnection("jdbc:sqlite:resources/database.db");
//            return con;
//        } catch (ClassNotFoundException | SQLException e) {
//            HttpServletResponse response = null;
//            response.getWriter().print("Problem with connection of database");
//            return null;
//        }
//    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        //testBazy(response);

        // response.getWriter().print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        String sqliteString = "jdbc:sqlite:C:\\Users\\Jarek\\IdeaProjects\\jsp2\\resources\\database.db"; //resources/database.db";
        Connection connection = null;// ConnecrDb();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            //Class.forName("org.sqlite.JDBC");
            DriverManager.registerDriver(new org.sqlite.JDBC());
            //response.getWriter().print("<br>w TRY CATCH");
            connection = DriverManager.getConnection(sqliteString);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from albums");
            //response.getWriter().print("<br>Result set: " + resultSet.next());
            response.getWriter().print("<table>");
            response.getWriter().print("<thead>");
            response.getWriter().print("<tr>");
            response.getWriter().print("<th>AlbumId</th>");
            response.getWriter().print("<th>Title</th>");
            response.getWriter().print("<th>ArtistId</th>");
            response.getWriter().print("</tr>");
            response.getWriter().print("</thead>");
            while (resultSet.next()) {
                //   response.getWriter().print("<br>w WHILE");
                response.getWriter().print("<tr>");
                response.getWriter().print("<td>" + resultSet.getInt(1) + "</td>");
                response.getWriter().print("<td>" + resultSet.getString(2) + "</td>");
                response.getWriter().print("<td>" + resultSet.getInt(3) + "</td>");
                response.getWriter().print("</tr>");
            }
            response.getWriter().print("</table>");
            //response.getWriter().print("<br>poza WHILE");
        } catch (SQLException e) {
            e.printStackTrace();
            response.getWriter().print("<br>Error: " + e.getErrorCode());
        } finally {
            try {
               // response.getWriter().print("<br>poza TRY z close()");
                resultSet.close();
                statement.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

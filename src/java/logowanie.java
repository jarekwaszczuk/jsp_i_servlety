import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "logowanie", value = "/logowanie")
public class logowanie extends HttpServlet {

    private String username = "jarek";
    private String password = "jarek";


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (username.equals(request.getParameter("user")) & password.equals(request.getParameter("pass"))) {
            request.getRequestDispatcher("zalogowany.jsp").forward(request, response);
        } else {
            request.setAttribute("bladLogowania", "Bledny login lub haslo (SERVLET)");
            request.getRequestDispatcher("logowanie.jsp").forward(request, response);
        }

    }
}

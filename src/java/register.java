import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "register", value = "/register")
public class register extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String errors = "";

        if ("".equals(request.getParameter("user"))) {
            errors = errors + "Brak użytkownika, ";
        }
        if (request.getParameter("pass") == null || request.getParameter("pass").length() < 8) {
            errors = errors + "brak hasła lub za krótkie, ";
        }
        if ("".equals(request.getParameter("email")) || !request.getParameter("email").contains("@") || !request.getParameter("email").contains(".")) {
            errors = errors + "brak lub błędny e-mail, ";
        }
        if ("".equals(request.getParameter("telephone"))) {
            errors = errors + "brak nr telefonu. ";
        }

        if (errors.length() == 0){
            request.getRequestDispatcher("registered.jsp").forward(request, response);
        } else{

            request.setAttribute("blad", "Błędy w formularzu (SERVLET): " + errors);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }


    }
}

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "licznik", value = "/licznik")
public class Licznik extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer odwiedzinyLiczba = 1;
        Cookie odwiedziny = new Cookie("odwiedziny", String.valueOf(1));

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookieLicznik : cookies) {
                if ("odwiedziny".equals(cookieLicznik.getName())) {
                    odwiedzinyLiczba = Integer.parseInt(cookieLicznik.getValue()) + 1;
                    odwiedziny = new Cookie("odwiedziny", String.valueOf(odwiedzinyLiczba));
                }
            }
        }
        response.addCookie(odwiedziny);

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().println("Jesteś na tej stronie po raz: " + odwiedzinyLiczba);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().print("to poszło po POST");
        response.getWriter().print("<br>");
        response.getWriter().print(request.getParameter("param1"));
        response.getWriter().print("<br>");
        response.getWriter().print(request.getParameter("param2"));
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().print("to poszło po PUT");
        response.getWriter().print("<br>");
        response.getWriter().print(request.getParameter("param1"));
        response.getWriter().print("<br>");
        response.getWriter().print(request.getParameter("param2"));
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().print("to poszło po DELETE");
        response.getWriter().print("<br>");
        response.getWriter().print(request.getParameter("param1"));
        response.getWriter().print("<br>");
        response.getWriter().print(request.getParameter("param2"));
    }

}

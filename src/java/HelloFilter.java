import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "HelloFilter", servletNames = "HelloServlet")
public class HelloFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        ServletContext context = request.getServletContext();

        HttpServletRequest req = (HttpServletRequest) request;

        context.log("Zadanie dla adresu: " + req.getRequestURI() + " parametry: " +  req.getQueryString());

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="Zadanie1", value="/Zadanie1")
public class Zadanie1 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        boolean stopkaIsVisible = false;
        if("y".equals(request.getParameter("stopka")) || "Y".equals(request.getParameter("stopka"))){
            stopkaIsVisible = true;
        }
        String nazwaFirmy = request.getParameter("name");
        String rok = request.getParameter("year");

        response.setContentType("text/html");

        response.getWriter().println(nazwaFirmy);
        response.getWriter().println("<br>");
        response.getWriter().println(rok);
        response.getWriter().println("<br>");

        if (stopkaIsVisible) {
            response.getWriter().println("<div name=\"stopka\" style=\"color: red;\">STOPKA WIDOCZNA" +
                    "</div>");
        }

    }
}
